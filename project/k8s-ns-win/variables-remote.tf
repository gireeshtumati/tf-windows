variable "module_prefix" {
  type        = string
  description = "Common resource name prefix"

  default = null
}

locals {
  module_prefix = element(concat(data.terraform_remote_state.base[*].outputs.module_prefix, [var.module_prefix]), 0)
}

variable "shared_tag" {
  type        = string
  description = "Common tag prefix for ASWU resources"

  default = null
}

locals {
  shared_tag = element(concat(data.terraform_remote_state.base[*].outputs.shared_tag, [var.shared_tag]), 0)
}

variable "permission_boundary" {
  type        = string
  description = "IAM permission boundary for roles"

  default = null
}

locals {
  permission_boundary = element(concat(data.terraform_remote_state.base[*].outputs.permission_boundary, [
    var.permission_boundary]), 0)
}

variable "k8s_cluster_name" {
  type        = string
  description = ""

  default = null
}

locals {
  k8s_cluster_name = element(concat(data.terraform_remote_state.base[*].outputs.k8s_cluster_name, [
    var.k8s_cluster_name]), 0)
}

variable "k8s_iam_nodes_role_arn" {
  type        = string
  description = ""

  default = null
}

locals {
  k8s_iam_nodes_role_arn = element(concat(data.terraform_remote_state.base[*].outputs.k8s_iam_nodes_role_arn, [
    var.k8s_iam_nodes_role_arn]), 0)
}

variable "env" {
  type        = object({
    iam_eks_admin_role = string
  })
  description = "Current environment configuration map"

  default = null
}

locals {
  env = element(concat(data.terraform_remote_state.base[*].outputs.env, [var.env]), 0)
}
