module "this" {
  source          = "../.."

  use_remote_state = false

  ec2_instance_type = var.ec2_instance_type
  name = "test"
}
