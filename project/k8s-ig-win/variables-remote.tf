variable "module_prefix" {
  type        = string
  description = "Common resource name prefix"

  default = null
}

locals {
  module_prefix = element(concat(data.terraform_remote_state.base[*].outputs.module_prefix, [var.module_prefix]), 0)
}

variable "env" {
  type        = object({account_id=string})
  description = "Current environment configuration map"

  default = null
}

locals {
  env = element(concat(data.terraform_remote_state.base[*].outputs.env, [var.env]), 0)
}



locals {
  aws_region = element(concat(data.terraform_remote_state.base[*].outputs.aws_region, [data.aws_region.current.name]), 0)
}

variable "shared_tag" {
  type        = string
  description = "Common tag prefix for ASWU resources"

  default = null
}

locals {
  shared_tag = element(concat(data.terraform_remote_state.base[*].outputs.shared_tag, [var.shared_tag]), 0)
}

variable "vpc_id" {
  type        = string
  description = "VPC id of subnets"

  default = null
}

locals {
  vpc_id = element(concat(data.terraform_remote_state.base[*].outputs.vpc_id, [var.vpc_id]), 0)
}

variable "vpc_subnets" {
  type        = list(string)
  description = "VPC subnets for instances"

  default = null
}

locals {
  vpc_subnets = element(concat(data.terraform_remote_state.base[*].outputs.vpc_subnets, [var.vpc_subnets]), 0)
}

variable "permission_boundary" {
  type        = string
  description = "IAM permission boundary for roles"

  default = null
}

locals {
  permission_boundary = element(concat(data.terraform_remote_state.base[*].outputs.permission_boundary, [
    var.permission_boundary]), 0)
}

variable "dns_zone" {
  type        = string
  description = "DNS zone for Route53 records"

  default = null
}

locals {
  dns_zone = element(concat(data.terraform_remote_state.base[*].outputs.dns_services_zone, [var.dns_zone]), 0)
}

variable "dns_suffix" {
  type        = string
  description = "DNS suffix for Route53 records"

  default = null
}

locals {
  dns_suffix = element(concat(data.terraform_remote_state.base[*].outputs.dns_services_suffix, [var.dns_suffix]), 0)
}

variable "certificate_arn" {
  type        = string
  description = "Certificate to use for ALB"

  default = null
}

locals {
  certificate_arn = element(concat(data.terraform_remote_state.base[*].outputs.certificate_services, [
    var.certificate_arn]), 0)
}

variable "ami_aws_linux" {
  type        = string
  description = "AMI of basic linux image"

  default = null
}

locals {
  ami_aws_linux = var.ami_aws_linux != null ? var.ami_aws_linux : (var.use_remote_state ? data.terraform_remote_state.base[0].outputs.ami_aws_linux : null)
}

variable "infra_bucket" {
  type        = string
  description = "Shared infrastructure bucket"

  default = null
}

locals {
  infra_bucket = element(concat(data.terraform_remote_state.base[*].outputs.infra_bucket, [var.infra_bucket]), 0)
}
