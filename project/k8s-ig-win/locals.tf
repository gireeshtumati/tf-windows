locals {
  create_resources = var.create ? 1 : 0

  # Service name grouping set of modules
  service = var.service != null ? var.service : var.name

  # Shared name of this module. Should be used as a prefix for submodules
  name = var.service != null ? "${var.service}-${var.name}" : var.name

  # Full module name with all prefixes (to be used as resource names)
  name_full = "${local.module_prefix}${var.project != null ? "${var.project}-" : ""}${local.name}"

  # Map of tags that should be used by submodules
  tags_common = merge(var.tags, {
    format(local.shared_tag, "extras/terraform-module/${var.module_name}/version") = var.module_version
  })

  # Map of tags used for resources created by this module
  tags = merge(local.tags_common, {
    format(local.shared_tag, "terraform-module") = var.module_name
    format(local.shared_tag, "service")          = local.service
    format(local.shared_tag, "role")             = "AWS EC2 linux instance group"
  })

  # For resources with limited amount of tags allowed (e.g. S3 objects)
  tags_short = {for k, v in local.tags : k => v if replace(k, format(local.shared_tag, "extras/"), "") == k}
}

locals {
  cloudwatch_log_group = var.create && var.logs_enable ? join("", aws_cloudwatch_log_group.this.*.name) : ""
}

locals {
  sg_id   = var.create ? (var.ec2_security_group_id != null ? var.ec2_security_group_id : aws_security_group.this[0].id) : null
  role_id = var.create ? (var.iam_role_name != null ? var.iam_role_name : aws_iam_role.this[0].name) : null

  # Tells cloud-init to run init script on each machine boot
  cloudconfig_parts_alwaysrun = [
    {
      filename     = "cloud-config.cfg"
      content_type = "text/cloud-config"
      content      = <<EOF
#cloud-config
cloud_final_modules:
- [scripts-user, always]
EOF
    }
  ]

  # Optional startup script
  cloudconfig_parts_additional_startup = var.startup_script != null ? [
    {
      content_type = "text/x-shellscript"
      content      = <<EOF
#!/usr/bin/env bash
set -xe

echo "Running additional startup script:"
${var.startup_script}
EOF
    },
  ] : []


  cloudconfig_parts_init = var.create ? [
    {
      content_type = "text/x-shellscript"
      content      = <<EOF
#!/usr/bin/env bash
set -xe
# Shared functions
${templatefile("${path.module}/data/functions.sh", {
  aws_region = local.aws_region
})}

# Init script
${templatefile("${path.module}/data/init.sh", {
  ssh_keys = concat(var.ssh_keys)
  aws_region = local.aws_region
  cloudwatch_log_group = local.cloudwatch_log_group
  docker_install = var.docker_install
  docker_install_cleanup = var.docker_install_cleanup
})}
EOF
    }
  ] : []
}
