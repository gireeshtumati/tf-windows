# terraform-module-ec2-ig

Creates EC2 instance group with IAM profile, SG and predefined userdata.

Relevant terraform resources:
* [aws_launch_configuration](https://www.terraform.io/docs/providers/aws/r/launch_configuration.html)
* [aws_autoscaling_group](https://www.terraform.io/docs/providers/aws/r/autoscaling_group.html)
* [aws_security_group](https://www.terraform.io/docs/providers/aws/r/aws_security_group.html)
* [aws_iam_instance_profile](https://www.terraform.io/docs/providers/aws/r/aws_iam_instance_profile.html)

## Examples

Examples of how to use these modules can be found in the [examples](examples) folder.
