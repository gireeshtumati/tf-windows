resource "aws_launch_configuration" "this" {
  count = local.create_resources

  name_prefix   = "${local.name_full}-"
  image_id      = local.ami_aws_linux
  instance_type = var.ec2_instance_type
  spot_price    = var.ec2_spot_price

  iam_instance_profile = aws_iam_instance_profile.this[0].id
  security_groups      = [local.sg_id]

  user_data = data.template_cloudinit_config.this[count.index].rendered

  lifecycle {
    create_before_destroy = true
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = var.ec2_root_volume_size
  }
}

resource "aws_placement_group" "this" {
  count = local.create_resources

  name     = local.name_full
  strategy = "spread"
}

locals {
  tags_instances = merge(local.tags, {
    "Name" = local.name_full,
  }, var.tags_instances)
}

resource "aws_autoscaling_group" "this" {
  count = local.create_resources

  name             = local.name_full
  min_size         = var.ec2_sg_min
  max_size         = var.ec2_sg_max
  desired_capacity = var.ec2_sg_min

  launch_configuration = aws_launch_configuration.this[0].id
  placement_group      = aws_placement_group.this[0].id

  lifecycle {
    ignore_changes = [desired_capacity]
  }

  vpc_zone_identifier = local.vpc_subnets


  tags = [for k, v in local.tags_instances : {
    key                 = k
    //noinspection HILConvertToHCL
    value               = "${v}"
    propagate_at_launch = "true"
  }]
}

resource "aws_security_group" "this" {
  count = local.create_resources * (var.ec2_security_group_id_override ? 1 : 0)

  name        = local.name_full
  description = "Security group for ${local.name_full} instances"
  vpc_id      = local.vpc_id

  tags = local.tags
}

resource "aws_security_group_rule" "this-egress" {
  count = local.create_resources * (var.ec2_security_group_id_override ? 1 : 0)

  type              = "egress"
  security_group_id = aws_security_group.this[0].id
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "this-ingress-ssh" {
  count = local.create_resources * (var.ec2_security_group_id_override ? 1 : 0)

  type              = "ingress"
  security_group_id = aws_security_group.this[0].id
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}
