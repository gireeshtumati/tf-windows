variable "create" {
  type        = bool
  description = "If we want to create module resources"

  default = true
}

variable "module_version" {
  type        = string
  description = "Version of the current module"

  default = "0.1.0-master"
}

variable "module_name" {
  type        = string
  description = "Name of the current module"

  default = "ec2-ig"
}

variable "project" {
  type        = string
  description = "Project name"

  default = null
}

variable "service" {
  type        = string
  description = "Service name. \"name\" field is used if omitted"

  default = null
}

variable "name" {
  type        = string
  description = "Resource name. If service parameter is used, it is added as a prefix"
}

variable "environment" {
  type        = string
  description = "Environment name"
}

variable "tags" {
  type        = map(string)
  description = "Map of common tags for all resources"

  default = {}
}

variable "use_remote_state" {
  type        = bool
  description = "If we need to use remote terraform-infra-base state"

  default = true
}

variable "tags_instances" {
  type        = map(string)
  description = "Additional tags for EC2 instances"

  default = {}
}

variable "tags_sg" {
  type        = map(string)
  description = "Additional tags for EC2 security group"

  default = {}
}

variable "iam_role_name" {
  type        = string
  description = "Overrides IAM role"

  default = null
}

variable "iam_role_name_override" {
  type = bool

  default = false
}

variable "ec2_security_group_id" {
  type        = string
  description = "Overrides security group"

  default = null
}

variable "ec2_security_group_id_override" {
  type = bool

  default = false
}

variable "ec2_instance_type" {
  type        = string
  description = "EC2 instance type"
}

variable "ec2_sg_min" {
  type        = number
  description = "EC2 minimal instance count"

  default = 1
}

variable "ec2_sg_max" {
  type        = number
  description = "EC2 maximum instance count"

  default = 1
}

variable "ec2_root_volume_size" {
  type        = number
  description = "EBS root volume size in gigabytes"

  default = 20
}

variable "docker_install" {
  type        = bool
  description = "If docker should be installed and configured"

  default = true
}

variable "docker_install_cleanup" {
  type        = bool
  description = "If docker cleanup image should be installed"

  default = true
}

variable "ssh_keys" {
  type        = list(string)
  description = "SSH keys to add to .ssh/authorized_keys"

  default = []
}

variable "startup_script" {
  type        = string
  description = "Additional bash startup script"

  default = null
}

variable "cloudconfig_parts" {
  type        = list(object({
    filename     = string,
    content      = string,
    # https://cloudinit.readthedocs.io/en/0.7.7/topics/format.html#mime-multi-part-archive
    content_type = string,
    # https://cloudinit.readthedocs.io/en/0.7.7/topics/merging.html
    merge_type   = string,
  }))
  description = "Additional cloudconfig parts"

  default = []
}

variable "logs_enable" {
  type        = bool
  description = "If Cloudwatch integration should be enabled"

  default = true
}

variable "logs_retention_days" {
  type        = number
  description = "For how long logs should be kept"

  default = 7
}

variable "iam_enable_ssm_access" {
  type        = bool
  description = "Enables SSM access"

  default = true
}

variable "ec2_spot_price" {
  type        = string
  description = "Spot price for instances"

  default = null
}
