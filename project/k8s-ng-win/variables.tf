variable "create" {
  type        = bool
  description = "If we want to create module resources"

  default = true
}

variable "module_version" {
  type        = string
  description = "Version of the current module"

  default = "0.1.0-master"
}

variable "module_name" {
  type        = string
  description = "Name of the current module"

  default = "k8s-ng"
}

variable "project" {
  type        = string
  description = "Project name"

  default = null
}

variable "service" {
  type        = string
  description = "Service name. \"name\" field is used if omitted"

  default = null
}

variable "name" {
  type        = string
  description = "Resource name. If service parameter is used, it is added as a prefix"
}

variable "environment" {
  type        = string
  description = "Environment name"
}

variable "tags" {
  type        = map(string)
  description = "Map of common tags for all resources"

  default = {}
}

variable "use_remote_state" {
  type        = bool
  description = "If we need to use remote terraform-infra-base state"

  default = true
}

variable "tags_instances" {
  type        = map(string)
  description = "Additional tags for EC2 instances"

  default = {}
}

variable "tags_sg" {
  type        = map(string)
  description = "Additional tags for EC2 security group"

  default = {}
}

variable "ec2_instance_type" {
  type        = string
  description = "EC2 instance type"
}

variable "ec2_sg_min" {
  type        = number
  description = "EC2 minimal instance count"

  default = 1
}

variable "ec2_sg_max" {
  type        = number
  description = "EC2 maximum instance count"

  default = 1
}

variable "ec2_root_volume_size" {
  type        = number
  description = "EBS root volume size in gigabytes"

  default = 20
}

variable "ssh_keys" {
  type        = list(string)
  description = "SSH keys to add to .ssh/authorized_keys"

  default = []
}

variable "k8s_node_labels" {
  type        = map(string)
  description = "Kubernetes node labels"

  default = {}
}

variable "k8s_node_taints" {
  type = list(object({
    key      = string
    value    = string
    effect   = string
  }))
  description = "Kubernetes node taints"

  default = []
}

variable "ec2_spot_price" {
  type        = string
  description = "Spot price for instances"

  default = null
}
