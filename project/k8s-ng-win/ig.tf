module "ig" {
  source = "../k8s-ig-win"

  create = var.create

  project     = var.project
  service     = var.service
  name        = var.name
  environment = var.environment

  tags           = var.tags
  tags_instances = merge(local.k8s_owned_map, var.tags_instances)
  tags_sg        = var.tags_sg

  ami_aws_linux        = local.ami_eks_linux
  ec2_instance_type    = var.ec2_instance_type
  ec2_sg_min           = var.ec2_sg_min
  ec2_sg_max           = var.ec2_sg_max
  ec2_root_volume_size = var.ec2_root_volume_size
  ec2_spot_price       = var.ec2_spot_price

  use_remote_state    = var.use_remote_state
  module_prefix       = local.module_prefix
  env                 = local.env  
  shared_tag          = local.shared_tag
  vpc_id              = local.vpc_id
  vpc_subnets         = local.vpc_subnets
  permission_boundary = local.permission_boundary
  infra_bucket        = local.infra_bucket
  ssh_keys            = var.ssh_keys

  ec2_security_group_id          = local.k8s_ec2_nodes_sg_id
  ec2_security_group_id_override = true

  iam_role_name          = local.k8s_iam_nodes_role_name
  iam_role_name_override = true

  startup_script = !var.create ? "" : <<EOF
#!/bin/bash
set -o xtrace
echo Cluster ARN is ${var.create ? data.aws_eks_cluster.target[0].arn : ""} # Used as trigger to recreate instances
/etc/eks/bootstrap.sh ${local.k8s_cluster_name == null ? "" : local.k8s_cluster_name} --kubelet-extra-args '${join(" ", local.system_args)}'
EOF
}
