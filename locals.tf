locals {
  k8s_ingress_dns_name = ( var.environment == "china-stage" ) ? "hilti-k8s-ingress-6991618df830926c.elb.cn-northwest-1.amazonaws.com.cn" : "hilti-k8s-ingress-8c3ff400510b02cf.elb.cn-northwest-1.amazonaws.com.cn" 

  tags = merge(local.tags_common, {
    format(local.shared_tag, "extras/terraform-module/${var.module_name}/version") = var.module_version

    format(local.shared_tag, "terraform-module") = var.module_name
    format(local.shared_tag, "project")          = var.project
  })
}
