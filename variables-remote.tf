data "terraform_remote_state" "base" {
  backend = "s3"
  config  = {
    bucket  = var.infra_bucket
    key     = "terraform/base.tfstate"
    region  = data.aws_region.current.name
    profile = "default"
  }
}



locals {
  tags_common = data.terraform_remote_state.base.outputs.tags_common
  shared_tag  = data.terraform_remote_state.base.outputs.shared_tag
}
